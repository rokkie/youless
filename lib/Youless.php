<?php

/**
 * @class Youless
 */
class Youless
{
  /**
   * @var string
   */
  protected static $PATTERN = '/^(?P<hour>\d+):(?P<min>\d+)\s+(?P<usage>\d+)\s*$/m';

  /**
   * @var array
   */
  protected static $PATH_MAP = array(
    'electricity' => 'V',
    'gas'         => 'W'
  );

  /**
   * @var string
   */
  protected $host;

  /**
   * Youless constructor.
   *
   * @param string $host  Network host of YouLess
   */
  public function __construct($host)
  {
    $this->host = $host;
  }

  /**
   * Fetch energy usage from YouLess
   *
   * @param  string   $type      electricity|gas
   * @param  DateTime $dateStart Date/Time to start
   * @return array               A list of entries specifying the usage over time
   */
  public function fetchUsage($type, DateTime $dateStart = null)
  {
    $fmt = 'http://%s/%s?w=%d';
    $w1  = file_get_contents(sprintf($fmt, $this->host, self::$PATH_MAP[$type], 1));
    $w2  = file_get_contents(sprintf($fmt, $this->host, self::$PATH_MAP[$type], 2));
    $w3  = file_get_contents(sprintf($fmt, $this->host, self::$PATH_MAP[$type], 3));

    $now = new DateTime();

    return array_reduce([$w3, $w2, $w1], function (array $all, $data) use ($now, $dateStart) {
      $count = preg_match_all(self::$PATTERN, $data, $matches);
      if (false === $count) { return $all; }

      $entries = [];
      for ($i = 0; $i < $count; $i++) {
        $hour  = intval($matches['hour'][$i], 10);
        $min   = intval($matches['min'][$i], 10);
        $usage = floatval($matches['usage'][$i]);

        $dateTime = (new DateTime('today'))->setTime($hour, $min, 0);
        if ($now < $dateTime) {
          $dateTime->sub(new DateInterval('P1D'));
        }

        if (!$dateStart || $dateStart < $dateTime) {
          $entries[] = [
            'dateTime' => $dateTime->format(DateTime::ISO8601),
            'usage'    => $usage,
          ];
        }
      }

      return array_merge($all, $entries);
    }, []);
  }
}
