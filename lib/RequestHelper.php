<?php

/**
 * @class RequestHelper
 */
class RequestHelper
{
  /**
   * Get a value from the querystring
   *
   * @param string $name    Name of the parameter to get
   * @param mixed  $default Default value if the parameter does not exist
   *
   * @return mixed
   */
  public static function getQueryValue($name, $default = null)
  {
    return array_key_exists($name, $_GET)
      ? $_GET[$name]
      : $default;
  }

  /**
   * Send data as JSON response
   *
   * @param mixed $data The data to send
   * @param int   $code HTTP response code
   */
  public static function sendJson($data, $code = 200)
  {
    header('Content-Type: application/json');
    http_response_code($code);

    echo json_encode($data, JSON_NUMERIC_CHECK);

    exit((200 <= $code && 300 > $code) ? 0 : 1);
  }
}
