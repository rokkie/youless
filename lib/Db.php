<?php

/**
 * @class Db
 */
class Db
{
  /**
   * @var PDO
   */
  protected $pdo;

  /**
   * @var array
   */
  private static $TABLE_MAP = [
    'electricity' => 'elektra',
    'gas'         => 'gas',
  ];

  /**
   * @var array
   */
  private static $SQL_MAP = [
    'insert_electricity' => 'INSERT INTO `elektra` (`date_time`, `watt_value`) VALUES (:dateTime, :usage)',
    'insert_gas'         => 'INSERT INTO `gas` (`date_time`, `cubic_meter`) VALUES (:dateTime, :usage)',
    'usage_electricity'  => "SELECT t.date_time, SUM(t.watt_value) AS 'usage' FROM `elektra` AS t",
    'usage_gas'          => "SELECT t.date_time, SUM(t.cubic_meter) AS 'usage' FROM `gas` AS t",
    'grain_min'          => 'GROUP BY t.date_time',
    'grain_hour'         => 'GROUP BY CONCAT(YEAR(t.date_time), DAYOFYEAR(t.date_time), HOUR(t.date_time))',
    'latest'             => 'SELECT MAX(`date_time`) FROM `%s`',
  ];

  /**
   * Db constructor.
   *
   * @param array $config Database configuration
   */
  public function __construct(array $config)
  {
    $dsn       = sprintf('mysql:host=%s;port=%s;dbname=%s', $config['host'], $config['port'], $config['name']);
    $this->pdo = new PDO($dsn, $config['user'], $config['pass'], [
      PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . $config['charset'],
    ]);
  }

  /**
   * Save energy usage to the database
   *
   * @param  string $type electricity|gas
   * @param  array  $data Usage data
   * @return boolean      Success
   */
  public function save($type, array $data)
  {
    $key     = sprintf('insert_%s', $type);
    $sql     = self::$SQL_MAP[$key];
    $stmt    = $this->pdo->prepare($sql);
    $success = true;

    foreach ($data as $row) {
      if (!$stmt->execute($row)) {
        $success = false;
      }
    }

    return $success;
  }

  /**
   * Find the latest date/time of the given type
   *
   * @param  string $type  electricity|gas
   * @return DateTime|null Date/Time of the latest entry of the given type
   */
  public function findLatest($type)
  {
    $table = self::$TABLE_MAP[$type];
    $sql   = sprintf(self::$SQL_MAP['latest'], $table);
    $stmt  = $this->pdo->prepare($sql);

    $stmt->execute();
    $latest = $stmt->fetchColumn();

    return !!$latest ? new DateTime($latest) : null;
  }

  /**
   * Fetch usage over time
   *
   * @param  string        $type        electricity|gas
   * @param  string        $granularity min|hour|day|week|month
   * @param  DateTime|null $dateStart   Date to start
   * @param  DateTime|null $dateEnd     Date to end
   * @return array
   */
  public function fetchUsage($type, $granularity = 'min', DateTime $dateStart = null, DateTime $dateEnd = null)
  {
    // start with a blank query
    $sql = [];

    // pick the right select based on the type
    $key   = sprintf('usage_%s', $type);
    $sql[] = self::$SQL_MAP[$key];

    // check if either start or end date are given
    if ($dateStart || $dateEnd) {
      // if start date was given, add where clause to the query
      if ($dateStart) {
        $sql[] = 'WHERE t.date_time >= :dateStart';
      }

      // if end date was given, add where/and clause to the query
      if ($dateEnd) {
        $sql[] = $dateStart
          ? 'AND t.date_time <= :dateEnd'
          : 'WHERE t.date_time <= :dateEnd';
      }
    }

    // pick the right group by based on the granularity
    $key   = sprintf('grain_%s', $granularity);
    $sql[] = self::$SQL_MAP[$key];

    // add order by to the query
    $sql[] = 'ORDER BY t.date_time ASC';

    // prepare the query and execute it
    $stmt = $this->pdo->prepare(implode(PHP_EOL, $sql));
    $stmt->execute(array_merge([],
      $dateStart ? ['dateStart' => $dateStart->format(DateTime::ISO8601)] : [],
      $dateEnd ? ['dateEnd' => $dateEnd->format(DateTime::ISO8601)] : []
    ));

    // return the results of the query
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
  }
}
