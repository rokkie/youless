<?php

/**
 * @class Import
 */
class Import
{
  /**
   * @var Db
   */
  protected $db;

  /**
   * @var Youless
   */
  protected $yl;

  /**
   * Import constructor.
   *
   * @param Db      $db Database object
   * @param Youless $yl Youless object
   */
  public function __construct(Db $db, Youless $yl)
  {
    $this->db = $db;
    $this->yl = $yl;
  }

  /**
   * Import usage data of a given type
   *
   * @param  string $type electricity|gas
   * @return boolean      Success
   */
  public function import($type)
  {
    $latest = $this->db->findLatest($type);
    $usage  = $this->yl->fetchUsage($type, $latest);

    return $this->db->save($type, $usage);
  }
}
